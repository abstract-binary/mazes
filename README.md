# Mazes

> Maze generators based on ["Mazes for Programmers" by Jamis Buck](https://pragprog.com/titles/jbmaze/)

See it live at [abstractbinary.org/mazes](https://abstractbinary.org/mazes).

This is a Phoenix server:

  * Install dependencies with `mix deps.get`
  * Install Node.js dependencies with `npm install` inside the `assets` directory
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.
