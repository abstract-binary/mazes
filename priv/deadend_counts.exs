# Run with `mix run priv/deadend_counts.exs`

tries = 500
size = 20

averages =
  for {name, mod} <- Mazes.algorithms(), reduce: %{} do
    averages ->
      IO.write("Running #{name}")

      total =
        for i <- 0..(tries - 1), reduce: 0 do
          total ->
            if Integer.mod(i, 20) == 0, do: IO.write(".")
            alias Mazes.Grid
            total + (Grid.init(size, size) |> mod.run() |> Grid.deadends() |> length)
        end

      IO.puts("")

      Map.put(averages, name, total / tries)
  end
  |> Enum.to_list()
  |> Enum.sort(fn {_, avg1}, {_, avg2} -> avg1 > avg2 end)

total_cells = size * size
IO.puts("")
IO.puts("Average dead-ends per #{size}x#{size} maze (#{total_cells} cells):")
IO.puts("")

for {name, average} <- averages do
  percentage = average * 100.0 / total_cells
  :io.fwrite("~21s : ~5.1f/~B (~.1f%)~n", [name, average, total_cells, percentage])
end
