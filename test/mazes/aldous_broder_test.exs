defmodule AldousBroderTest do
  use Mazes.MazeCase, async: true

  test "the algo runs" do
    x = Grid.init(2, 3)
    assert(AldousBroder.run(x) != :error)
  end

  test "masked grid" do
    mask =
      Mask.init(2, 3)
      |> Mask.put({0, 0}, false)
      |> Mask.put({0, 1}, false)

    x = Grid.init(2, 3, mask)
    assert(AldousBroder.run(x) != :error)
  end
end
