defmodule DistancesTest do
  use Mazes.MazeCase, async: true

  test "path_to" do
    g = sample_grid1()

    a = g[{0, 0}]

    distances = Cell.distances(a, g)
    test = fn goal -> Distances.path_to(distances, goal, g).cells end
    assert(test.({0, 1}) == %{{0, 0} => 0, {0, 1} => 1})
    assert(test.({0, 2}) == %{{0, 0} => 0, {0, 1} => 1, {0, 2} => 2})
    assert(test.({1, 2}) == %{{0, 0} => 0, {1, 0} => 1, {1, 1} => 2, {1, 2} => 3})
  end

  test "path_to fail" do
    g =
      sample_grid1()
      |> Grid.unlink({1, 1}, {1, 2})

    distances = Cell.distances({0, 0}, g)
    assert({:error, _} = Distances.path_to(distances, {1, 2}, g))
  end

  test "max" do
    g = sample_grid1()
    distances = Cell.distances({0, 0}, g)
    assert(Distances.max(distances) == {{1, 2}, 3})
  end
end
