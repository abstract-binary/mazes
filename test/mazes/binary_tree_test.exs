defmodule BinaryTreeTest do
  use Mazes.MazeCase, async: true

  test "the algo runs" do
    x = Grid.init(2, 3)
    assert(BinaryTree.run(x) != :error)
  end
end
