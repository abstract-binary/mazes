defmodule CellTest do
  use Mazes.MazeCase, async: true

  test "cell accessors" do
    cell = Cell.init(2, 3)
    assert(cell.row == 2)
    assert(cell.column == 3)
  end

  test "cell links" do
    a = Cell.init(1, 2)
    b = Cell.init(3, 4)
    a = Cell.link(a, b)
    b = Cell.link(b, a)
    assert(Cell.linked?(a, b))
    assert(Cell.linked?(b, a))
  end

  test "distances" do
    g =
      Grid.init(2, 3)
      |> Grid.link({0, 0}, {0, 1})
      |> Grid.link({0, 1}, {0, 2})
      |> Grid.link({0, 0}, {1, 0})
      |> Grid.link({1, 0}, {1, 1})
      |> Grid.link({1, 1}, {1, 2})

    a = g[{0, 0}]

    distances = Cell.distances(a, g)
    assert(distances[a] == 0)
    assert(distances[{0, 1}] == 1)
    assert(distances[{0, 2}] == 2)
    assert(distances[{1, 0}] == 1)
    assert(distances[{1, 1}] == 2)
    assert(distances[{1, 2}] == 3)
  end
end
