defmodule GridTest do
  use Mazes.MazeCase, async: true

  test "grid size" do
    grid = Grid.init(2, 3)
    assert(Grid.length(grid) == 6)
  end

  test "cell neighbours" do
    grid = Grid.init(2, 3)
    assert(grid[{0, 0}].north == nil)
    assert(grid[{0, 0}].west == nil)
    assert(grid[{0, 0}].east == {0, 1})
    assert(grid[{0, 0}].south == {1, 0})
    assert(grid[{1, 2}].north == {0, 2})
    assert(grid[{1, 2}].west == {1, 1})
    assert(grid[{1, 2}].east == nil)
    assert(grid[{1, 2}].south == nil)
  end

  test "cell oob fails" do
    grid = Grid.init(2, 3)
    assert_raise(ArgumentError, fn -> grid[{100, 100}] end)
  end

  test "all cell neighbours exist" do
    grid = Grid.init(2, 3)

    Grid.map_cells(grid, fn cell ->
      assert(grid[cell] != nil)

      for neighbour <- [cell.north, cell.west, cell.south, cell.east], neighbour != nil do
        assert(grid[neighbour] != nil)
      end
    end)
  end

  test "cell links" do
    grid = Grid.init(2, 3)

    grid =
      grid
      |> Grid.link({0, 0}, {1, 0})
      |> Grid.link({1, 1}, {1, 2})

    assert(Cell.linked?(grid[{0, 0}], grid[{1, 0}]))
    assert(Cell.linked?(grid[{1, 0}], grid[{0, 0}]))
    assert(not Cell.linked?(grid[{0, 0}], grid[{0, 1}]))
    assert(Cell.linked?(grid[{1, 1}], grid[{1, 2}]))
    assert(Cell.linked?(grid[{1, 2}], grid[{1, 1}]))
    grid = Grid.unlink(grid, {1, 1}, {1, 2})
    assert(not Cell.linked?(grid[{1, 1}], grid[{1, 2}]))
    assert(not Cell.linked?(grid[{1, 2}], grid[{1, 1}]))
  end

  test "longest path" do
    g = sample_grid1()
    assert(Grid.longest_path(g) == {{1, 2}, {0, 2}})
  end
end
