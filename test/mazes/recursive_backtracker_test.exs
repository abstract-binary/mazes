defmodule RecursiveBacktrackerTest do
  use Mazes.MazeCase, async: true

  test "the algo runs" do
    x = Grid.init(2, 3)
    assert(RecursiveBacktracker.run(x) != :error)
  end
end
