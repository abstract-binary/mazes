defmodule MaskTest do
  use Mazes.MazeCase, async: true

  test "empty mask access" do
    mask = Mask.init(2, 3)
    assert(mask[{0, 0}] == true)
    assert(mask[{0, 1}] == true)
    assert(mask[{0, 2}] == true)
    assert(mask[{1, 0}] == true)
    assert(mask[{1, 1}] == true)
    assert(mask[{1, 2}] == true)
  end

  test "mask oob access succeeds with false" do
    mask = Mask.init(2, 3)

    assert(mask[{2, 0}] == false)
    assert(mask[{0, 3}] == false)
    assert(mask[{-1, 0}] == false)
    assert(mask[{0, -1}] == false)
  end

  test "mask update fails" do
    mask = Mask.init(2, 3)
    assert_raise(UndefinedFunctionError, fn -> put_in(mask[{1, 1}], true) end)
  end

  test "mask put works" do
    mask = Mask.init(2, 3)
    assert(mask[{1, 1}] == true)
    mask = Mask.put(mask, {1, 1}, false)
    assert(mask[{1, 1}] == false)
  end

  test "mask put with wrong type fails" do
    mask = Mask.init(2, 3)
    assert_raise(FunctionClauseError, fn -> Mask.put(mask, {1, 1}, nil) end)
  end

  test "mask put oob fails" do
    mask = Mask.init(2, 3)
    assert_raise(ArgumentError, fn -> Mask.put(mask, {10, 10}, false) end)
  end

  test "mask length works" do
    mask = Mask.init(2, 3)
    assert(Mask.length(mask) == 6)
    mask = Mask.put(mask, {1, 1}, false)
    mask = Mask.put(mask, {0, 1}, false)
    assert(Mask.length(mask) == 4)
    mask = Mask.put(mask, {0, 1}, false)
    assert(Mask.length(mask) == 4)
    mask = Mask.put(mask, {0, 1}, true)
    assert(Mask.length(mask) == 5)
    mask = Mask.put(mask, {1, 2}, true)
    assert(Mask.length(mask) == 5)
  end

  test "random cell always returns" do
    mask = Mask.init(2, 3)
    assert(Mask.random_cell(mask) != nil)

    mask =
      for row <- 0..1, column <- 0..2, reduce: mask do
        mask -> Mask.put(mask, {row, column}, false)
      end

    assert(Mask.length(mask) == 0)
    assert(Mask.random_cell(mask) == nil)
  end
end
