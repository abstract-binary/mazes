defmodule MazesWeb.PageLiveTest do
  use MazesWeb.ConnCase

  import Phoenix.LiveViewTest

  test "disconnected and connected render", %{conn: conn} do
    {:ok, page_live, disconnected_html} = live(conn, "/")
    assert disconnected_html =~ "Mazes"
    assert render(page_live) =~ "Maze options"
  end
end
