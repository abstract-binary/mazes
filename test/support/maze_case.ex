defmodule Mazes.MazeCase do
  @moduledoc false

  use ExUnit.CaseTemplate

  using do
    quote do
      alias Mazes.{
        AldousBroder,
        BinaryTree,
        Cell,
        Distances,
        Grid,
        HuntAndKill,
        Mask,
        RecursiveBacktracker,
        Sidewinder,
        Wilson
      }

      @doc """
      ```
      A -> B -> C
      v
      D -> E -> F
      ```
      """
      def sample_grid1 do
        Grid.init(2, 3)
        |> Grid.link({0, 0}, {0, 1})
        |> Grid.link({0, 1}, {0, 2})
        |> Grid.link({0, 0}, {1, 0})
        |> Grid.link({1, 0}, {1, 1})
        |> Grid.link({1, 1}, {1, 2})
      end
    end
  end
end
