defmodule MazesWeb.PageLive do
  @moduledoc false

  use MazesWeb, :live_view

  alias Mazes.{Cell, Distances, Grid}
  require Logger

  @default_rows 5
  @default_columns 5
  @cell_size 30

  @impl true
  def mount(_params, _session, socket) do
    {default_algorithm, _} = hd(Mazes.algorithms())

    socket =
      socket
      |> assign(live_url: Application.fetch_env!(:mazes, :live_url))
      |> assign(
        algorithm: default_algorithm,
        rows: @default_rows,
        columns: @default_columns,
        grid:
          Grid.init(@default_rows, @default_columns)
          |> algorithm_module(default_algorithm).run(),
        cell_size: @cell_size,
        origin: nil,
        distances: nil,
        action: :show_distances
      )

    {:ok, socket}
  end

  @impl true
  def handle_event(
        "generate",
        %{"algorithm" => algorithm, "rows" => rows, "columns" => columns},
        socket
      ) do
    algorithm_mod = algorithm_module(algorithm)

    rows = parse_form_integer(rows, @default_rows)
    columns = parse_form_integer(columns, @default_columns)
    old_grid = socket.assigns.grid

    grid =
      if old_grid.rows == rows and old_grid.columns == columns do
        Grid.init(rows, columns, old_grid.mask)
      else
        Grid.init(rows, columns)
      end

    socket =
      socket
      |> assign(algorithm: algorithm, rows: rows, columns: columns)
      |> assign(grid: algorithm_mod.run(grid))
      |> compute_distances()

    {:noreply, socket}
  end

  @impl true
  def handle_event("grid-clicked", %{"row" => row, "col" => col}, socket) do
    with {:ok, row} <- integer_parse(row),
         {:ok, col} <- integer_parse(col) do
      old_origin = socket.assigns.origin

      socket =
        socket
        |> assign(origin: nil)
        |> compute_distances()

      case socket.assigns.action do
        :show_distances ->
          if old_origin == {row, col} do
            # Clicking on the same cell twice removes the distances
            {:noreply, socket}
          else
            socket =
              socket
              |> assign(origin: {row, col})
              |> compute_distances()

            {:noreply, socket}
          end

        :edit_mask ->
          socket =
            socket
            |> assign(grid: Grid.toggle_mask(socket.assigns.grid, {row, col}))

          {:noreply, socket}
      end
    else
      error ->
        {:noreply, put_flash(socket, :error, inspect(error))}
    end
  end

  @impl true
  def handle_event("set-action", %{"action" => action}, socket) do
    socket =
      socket
      |> assign(action: String.to_existing_atom(action))

    {:noreply, socket}
  end

  defp compute_distances(socket) do
    case socket.assigns.origin do
      nil ->
        assign(socket, distances: nil)

      origin ->
        case Cell.distances(origin, socket.assigns.grid) do
          {:error, reason} ->
            socket
            |> assign(distances: nil)
            |> put_flash(:error, reason)

          distances ->
            assign(socket, distances: distances)
        end
    end
  end

  defp parse_form_integer(str, default) when is_binary(str) and is_integer(default) do
    case str do
      "" ->
        default

      _ ->
        case Integer.parse(str) do
          {n, ""} -> n
          _ -> default
        end
    end
  end

  defp integer_parse(str) do
    case Integer.parse(str) do
      {n, ""} -> {:ok, n}
      _ -> {:error, "not an integer: '#{inspect(str)}'"}
    end
  end

  @spec algorithm_module(String.t()) :: atom()
  defp algorithm_module(algo) when is_binary(algo) do
    {_, mod} =
      Enum.find(Mazes.algorithms(), hd(Mazes.algorithms()), fn {key, _} -> algo == key end)

    mod
  end

  defp selected_attr(x, x), do: "selected=\"selected\""
  defp selected_attr(_, _), do: ""

  defp svg_height(%Grid{} = grid), do: (1 + grid.rows) * @cell_size
  defp svg_width(%Grid{} = grid), do: (1 + grid.columns) * @cell_size

  defp svg_lines(%Grid{} = grid) do
    for row <- 0..(grid.rows - 1), col <- 0..(grid.columns - 1), reduce: [] do
      acc ->
        cell = grid[{row, col}]
        x1 = cell.column * @cell_size
        y1 = cell.row * @cell_size
        x2 = (cell.column + 1) * @cell_size
        y2 = (cell.row + 1) * @cell_size

        acc = if is_nil(cell.north), do: [{x1, y1, x2, y1} | acc], else: acc
        acc = if is_nil(cell.west), do: [{x1, y1, x1, y2} | acc], else: acc
        acc = if Cell.linked?(cell, cell.east), do: acc, else: [{x2, y1, x2, y2} | acc]
        acc = if Cell.linked?(cell, cell.south), do: acc, else: [{x1, y2, x2, y2} | acc]
        acc
    end
  end

  defp svg_grid_color(%Grid{} = grid, distances, {_, _} = key) do
    if grid.mask[key],
      do: svg_distances_color(distances, key),
      else: "#333333"
  end

  defp svg_distances_color(nil, {_, _}), do: "transparent"

  defp svg_distances_color(%Distances{} = distances, {_, _} = key) do
    case distances[key] do
      nil ->
        "black"

      distance ->
        maximum = max_distance(distances)
        intensity = (maximum - distance) / (1 + maximum)
        dark = Float.round(255 * intensity)
        bright = 100 + Float.round(150 * intensity)
        "rgb(#{dark}, #{bright}, #{dark})"
    end
  end

  defp max_distance(nil), do: 0

  defp max_distance(%Distances{} = distances) do
    {_, maximum} = Distances.max(distances)
    maximum
  end

  defp available_algorithms, do: Enum.map(Mazes.algorithms(), fn {name, _} -> name end)

  defp deadends(%Grid{} = grid), do: length(Grid.deadends(grid))
end
