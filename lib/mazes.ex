defmodule Mazes do
  @moduledoc """
  Mazes keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  @spec algorithms() :: [{String.t(), atom()}]
  def algorithms do
    [
      {"Hunt-and-Kill", Mazes.HuntAndKill},
      {"Recursive Backtracker", Mazes.RecursiveBacktracker},
      {"Wilson", Mazes.Wilson},
      {"Aldous-Broder", Mazes.AldousBroder},
      {"Binary Tree", Mazes.BinaryTree},
      {"Sidewinder", Mazes.Sidewinder}
    ]
  end
end
