defmodule Mazes.Algorithm do
  @moduledoc """
  The shared interface of all algorithms.
  """

  alias Mazes.Grid

  @callback run(Grid.t()) :: Grid.t()
end
