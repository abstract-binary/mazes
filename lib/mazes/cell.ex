defmodule Mazes.Cell do
  @moduledoc false

  alias __MODULE__
  alias Mazes.{Distances, Grid}

  @enforce_keys [:row, :column]
  defstruct [:row, :column, :links, :north, :south, :west, :east]

  @type key :: {non_neg_integer(), non_neg_integer()}
  @type t :: %Cell{
          row: non_neg_integer(),
          column: non_neg_integer(),
          links: %{required(key()) => true},
          north: key() | nil,
          south: key() | nil,
          west: key() | nil,
          east: key() | nil
        }

  @spec init(non_neg_integer(), non_neg_integer()) :: Cell.t()
  def init(row, column) when row >= 0 and column >= 0 do
    %Cell{row: row, column: column, links: %{}}
  end

  @spec key(Cell.t()) :: key
  def key(%Cell{} = t), do: {t.row, t.column}

  @spec neighbours(Cell.t()) :: [key]
  def neighbours(%Cell{} = t) do
    [t.north, t.south, t.west, t.east]
    |> Enum.filter(&(&1 != nil))
  end

  @spec visited_neighbours(Cell.t(), Grid.t()) :: [key]
  def visited_neighbours(%Cell{} = t, %Grid{} = grid) do
    t
    |> neighbours()
    |> Enum.filter(&(not Enum.empty?(links(grid[&1]))))
  end

  @spec unvisited_neighbours(Cell.t(), Grid.t()) :: [key]
  def unvisited_neighbours(%Cell{} = t, %Grid{} = grid) do
    t
    |> neighbours()
    |> Enum.filter(&Enum.empty?(links(grid[&1])))
  end

  @spec link(Cell.t(), Cell.t()) :: Cell.t()
  def link(%Cell{} = t, %Cell{} = other) do
    %{t | links: Map.put(t.links, key(other), true)}
  end

  @spec unlink(Cell.t(), Cell.t()) :: Cell.t()
  def unlink(%Cell{} = t, %Cell{} = other) do
    %{t | links: Map.delete(t.links, key(other))}
  end

  @spec linked?(Cell.t(), nil | key | Cell.t()) :: boolean()
  def linked?(%Cell{}, nil), do: false
  def linked?(%Cell{} = t, {_, _} = other), do: Map.has_key?(t.links, other)
  def linked?(%Cell{} = t, %Cell{} = other), do: Map.has_key?(t.links, key(other))

  @spec links(Cell.t()) :: [key]
  def links(%Cell{} = t), do: Map.keys(t.links)

  @doc """
  Runs a BFS from the given cell and returns the `%Distances{}` to every
  other reachable cell.
  """
  @spec distances(Cell.t() | key, Grid.t()) :: Distances.t() | {:error, String.t()}
  def distances(%Cell{} = t, %Grid{} = grid), do: distances([t], Distances.init(t), grid)

  def distances({_, _} = key, %Grid{} = grid) do
    case grid[key] do
      nil -> {:error, "cell not found in grid"}
      cell -> distances(cell, grid)
    end
  end

  defp distances([], %Distances{} = distances, %Grid{}), do: distances

  defp distances(frontier, %Distances{} = distances, %Grid{} = grid) do
    {new_frontier, distances} =
      for cell <- frontier, reduce: {[], distances} do
        {new_frontier, distances} ->
          for linked_key <- links(cell), reduce: {new_frontier, distances} do
            {new_frontier, distances} ->
              linked = grid[linked_key]

              if distances[linked],
                do: {new_frontier, distances},
                else: {[linked | new_frontier], put_in(distances[linked], distances[cell] + 1)}
          end
      end

    distances(new_frontier, distances, grid)
  end
end
