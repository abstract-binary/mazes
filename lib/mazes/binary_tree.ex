defmodule Mazes.BinaryTree do
  @moduledoc false
  @behaviour Mazes.Algorithm

  alias Mazes.{Cell, Grid}

  @impl true
  def run(%Grid{} = grid) do
    for row <- 0..(grid.rows - 1), col <- 0..(grid.columns - 1), reduce: grid do
      grid ->
        cell = grid[{row, col}]

        [cell.north, cell.east]
        |> Enum.filter(&(not is_nil(&1)))
        |> case do
          [] -> grid
          neighbours -> Grid.link(grid, Cell.key(cell), Enum.random(neighbours))
        end
    end
  end
end
