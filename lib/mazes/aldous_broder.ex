defmodule Mazes.AldousBroder do
  @moduledoc false
  @behaviour Mazes.Algorithm

  alias Mazes.{Cell, Grid}

  @impl true
  def run(%Grid{} = grid) do
    case Grid.random_cell(grid) do
      nil ->
        grid

      cell ->
        unvisited = Grid.length(grid) - 1
        loop(grid, cell, unvisited, 10 * unvisited)
    end
  end

  defp loop(%Grid{} = grid, %Cell{}, 0, _), do: grid
  defp loop(%Grid{} = grid, %Cell{}, _, 0), do: grid

  defp loop(%Grid{} = grid, %Cell{} = cell, unvisited, max_iterations)
       when is_integer(unvisited) and is_integer(max_iterations) do
    neighbour = grid[Enum.random(Cell.neighbours(cell))]

    case Cell.links(neighbour) do
      [] ->
        loop(
          Grid.link(grid, Cell.key(cell), Cell.key(neighbour)),
          neighbour,
          unvisited - 1,
          max_iterations - 1
        )

      _ ->
        loop(grid, neighbour, unvisited, max_iterations - 1)
    end
  end
end
