defmodule Mazes.Mask do
  @moduledoc false

  alias __MODULE__
  alias Mazes.Cell

  @enforce_keys [:rows, :columns, :grid]
  defstruct [:rows, :columns, :grid]

  @type t :: %Mask{
          rows: pos_integer(),
          columns: pos_integer(),
          grid: %{required(Cell.key()) => boolean()}
        }

  @spec init(pos_integer(), pos_integer()) :: Mask.t()
  def init(rows, columns)
      when is_integer(rows) and rows > 0 and is_integer(columns) and columns > 0 do
    %Mask{rows: rows, columns: columns, grid: %{}}
  end

  @spec fetch(Mask.t(), Cell.key() | Cell.t()) :: {:ok, boolean()}
  def fetch(%Mask{} = t, {_, _} = key) do
    if is_oob(t, key),
      do: {:ok, false},
      else: {:ok, Map.get(t.grid, key, true)}
  end

  def fetch(%Mask{} = t, %Cell{} = cell), do: fetch(t, Cell.key(cell))

  @doc """
  Update the mask.
  """
  @spec put(Mask.t(), Cell.key(), boolean()) :: Mask.t()
  def put(%Mask{} = t, {_, _} = key, value) when is_boolean(value) do
    check_oob(t, key)
    put_in(t.grid[key], value)
  end

  @doc """
  Returns the number of enabled/true/unmasked cells.
  """
  @spec length(Mask.t()) :: non_neg_integer()
  def length(%Mask{} = t) do
    for {_, false} <- t.grid, reduce: t.rows * t.columns do
      enabled -> enabled - 1
    end
  end

  @spec random_cell(Mask.t()) :: Cell.key() | nil
  def random_cell(%Mask{} = t), do: random_cell(t, 0)

  defp random_cell(%Mask{} = t, 10 = attempt) do
    if Mask.length(t) == 0,
      do: nil,
      else: random_cell(%Mask{} = t, attempt + 1)
  end

  defp random_cell(%Mask{} = t, attempt) when is_integer(attempt) do
    key = {Enum.random(0..(t.rows - 1)), Enum.random(0..(t.columns - 1))}

    case t[key] do
      true -> key
      false -> random_cell(t, attempt + 1)
    end
  end

  defp is_oob(%Mask{} = t, {row, column}) do
    row < 0 or row >= t.rows or column < 0 or column >= t.columns
  end

  @spec check_oob(Mask.t(), Cell.key()) :: nil | no_return()
  defp check_oob(%Mask{} = t, {_, _} = key) do
    if is_oob(t, key),
      do: raise(ArgumentError, "key #{inspect(key)} out of bounds")
  end
end
