defmodule Mazes.Grid do
  @moduledoc false

  alias __MODULE__
  alias Mazes.{Cell, Distances, Mask}

  @enforce_keys [:rows, :columns, :grid, :mask]
  defstruct [:rows, :columns, :grid, :mask]

  @type t :: %Grid{
          rows: pos_integer(),
          columns: pos_integer(),
          grid: %{required(Cell.key()) => Cell.t()},
          mask: Mask.t()
        }

  @spec init(pos_integer(), pos_integer(), Mask.t() | nil) :: Grid.t()
  def init(rows, columns, mask \\ nil)
      when is_integer(rows) and rows > 0 and is_integer(columns) and columns > 0 do
    mask = mask || Mask.init(rows, columns)

    grid =
      for row <- 0..(rows - 1), column <- 0..(columns - 1), into: %{} do
        cell = Cell.init(row, column)
        {Cell.key(cell), cell}
      end

    t = %Grid{rows: rows, columns: columns, grid: grid, mask: mask}
    configure_cells(t)
  end

  @spec length(Grid.t()) :: non_neg_integer()
  def length(%Grid{} = t), do: Mask.length(t.mask)

  @spec fetch(Grid.t(), Cell.key() | Cell.t()) :: {:ok, Cell.t() | nil}
  def fetch(%Grid{} = t, {_, _} = key) do
    check_oob(t, key)
    {:ok, Map.get(t.grid, key, nil)}
  end

  def fetch(%Grid{} = t, %Cell{} = cell), do: fetch(t, Cell.key(cell))

  @spec link(Grid.t(), Cell.key(), Cell.key()) :: Grid.t()
  def link(%Grid{} = t, {_, _} = key1, {_, _} = key2) do
    cell1 = t[key1]
    cell2 = t[key2]

    put_in(t.grid, %{
      t.grid
      | key1 => Cell.link(cell1, cell2),
        key2 => Cell.link(cell2, cell1)
    })
  end

  @spec unlink(Grid.t(), Cell.key(), Cell.key()) :: Grid.t()
  def unlink(%Grid{} = t, {_, _} = key1, {_, _} = key2) do
    cell1 = t[key1]
    cell2 = t[key2]

    put_in(t.grid, %{
      t.grid
      | key1 => Cell.unlink(cell1, cell2),
        key2 => Cell.unlink(cell2, cell1)
    })
  end

  @spec toggle_mask(Grid.t(), Cell.key()) :: Grid.t()
  def toggle_mask(%Grid{} = t, {_, _} = key) do
    put_in(t.mask, Mask.put(t.mask, key, not t.mask[key]))
  end

  @spec cells(Grid.t()) :: [Cell.key()]
  def cells(%Grid{} = t) do
    for {key, _} <- t.grid, do: key
  end

  @spec random_cell(Grid.t()) :: Cell.t() | nil
  def random_cell(%Grid{} = t) do
    case Mask.random_cell(t.mask) do
      nil -> nil
      key -> t[key]
    end
  end

  @spec map_cells(Grid.t(), (Cell.t() -> Cell.t())) :: Grid.t()
  def map_cells(%Grid{} = t, f) do
    put_in(t.grid, for({key, cell} <- t.grid, into: %{}, do: {key, f.(cell)}))
  end

  @doc "Pretty prints the grid."
  @spec to_string(Grid.t(), (Cell.t() -> String.t())) :: String.t()
  def to_string(%Grid{} = t, cell_contents \\ fn _ -> " " end) do
    output =
      for _ <- 0..(t.columns - 1), reduce: "+" do
        acc -> acc <> "---+"
      end

    output = output <> "\n"

    for row <- 0..(t.rows - 1), reduce: output do
      output ->
        {top, bottom} =
          for column <- 0..(t.columns - 1), reduce: {"|", "+"} do
            {top, bottom} ->
              {east_boundary, south_boundary, cell_contents} =
                if cell = t[{row, column}] do
                  {if(Cell.linked?(cell, cell.east), do: " ", else: "|"),
                   if(Cell.linked?(cell, cell.south), do: "   ", else: "---"),
                   cell_contents.(cell)}
                else
                  # This shouldn't ever happen
                  {"zzz", "zzz", "z"}
                end

              {top <> " #{cell_contents} " <> east_boundary, bottom <> south_boundary <> "+"}
          end

        output <> top <> "\n" <> bottom <> "\n"
    end
  end

  @doc """
  Pretty prints the grid with distances from `origin` as the contents of
  the cells. If `goal` is given, then only the shortest path from `origin`
  to `goal` will be shown.
  """
  @spec to_string_distances(Grid.t(), Cell.key(), Cell.key() | nil) :: String.t()
  def to_string_distances(%Grid{} = t, {_, _} = origin, goal \\ nil) do
    distances = Cell.distances(origin, t)

    distances =
      case goal do
        nil -> distances
        {_, _} -> Distances.path_to(distances, goal, t)
      end

    to_string(t, fn cell ->
      case distances[cell] do
        nil -> " "
        n when is_integer(n) -> Integer.to_string(n, 36)
      end
    end)
  end

  @doc "Finds the longest path in the grid."
  @spec longest_path(Grid.t()) :: {Cell.key(), Cell.key()}
  def longest_path(%Grid{} = t) do
    d = Cell.distances({0, 0}, t)
    {new_start, _} = Distances.max(d)
    d = Cell.distances(new_start, t)
    {furthest_cell, _} = Distances.max(d)
    {new_start, furthest_cell}
  end

  @spec to_string_longest_path(Grid.t()) :: String.t()
  def to_string_longest_path(%Grid{} = t) do
    {origin, goal} = longest_path(t)
    to_string_distances(t, origin, goal)
  end

  @spec deadends(Grid.t()) :: [Cell.key()]
  def deadends(%Grid{} = t) do
    for {_, cell} <- t.grid, Kernel.length(Cell.links(cell)) == 1, do: cell
  end

  defp configure_cells(%Grid{} = t) do
    map_cells(t, fn cell ->
      row = cell.row
      col = cell.column

      case t.mask[{row, col}] do
        false ->
          cell

        true ->
          key_or_nil = fn row, col ->
            case t.mask[{row, col}] do
              true -> {row, col}
              false -> nil
            end
          end

          %{
            cell
            | north: key_or_nil.(row - 1, col),
              south: key_or_nil.(row + 1, col),
              west: key_or_nil.(row, col - 1),
              east: key_or_nil.(row, col + 1)
          }
      end
    end)
  end

  @spec check_oob(Grid.t(), Cell.key()) :: nil | no_return()
  defp check_oob(%Grid{} = t, {row, column} = key) do
    if row < 0 or row >= t.rows or column < 0 or column >= t.columns,
      do: raise(ArgumentError, "key #{inspect(key)} out of bounds")
  end
end
