defmodule Mazes.Distances do
  @moduledoc """
  A wrapper around a `%Cell{} => integer` map with some useful functions.

  You will probably get an instance of this module by calling `Cell.distances`.
  """

  @behaviour Access

  alias __MODULE__
  alias Mazes.{Cell, Grid}

  @enforce_keys [:root, :cells]
  defstruct [:root, :cells]

  @type t :: %Distances{
          root: Cell.key(),
          cells: %{required(Cell.key()) => non_neg_integer()}
        }

  @spec init(Cell.key() | Cell.t()) :: Distances.t()
  def init({_, _} = root) do
    %Distances{root: root, cells: %{root => 0}}
  end

  def init(%Cell{} = root), do: init(Cell.key(root))

  @impl true
  def fetch(%Distances{cells: cells}, {_, _} = key), do: Map.fetch(cells, key)

  @impl true
  def fetch(%Distances{} = t, %Cell{} = cell), do: fetch(t, Cell.key(cell))

  @impl true
  def get_and_update(%Distances{} = t, {_, _} = key, value) do
    {res, cells} = Map.get_and_update(t.cells, key, value)
    {res, %{t | cells: cells}}
  end

  @impl true
  def get_and_update(%Distances{} = t, %Cell{} = cell, value) do
    get_and_update(t, Cell.key(cell), value)
  end

  @impl true
  def pop(%Distances{} = t, key) do
    {res, cells} = Map.pop(t.cells, key)
    {res, %{t | cells: cells}}
  end

  @spec mem(Distances.t(), Cell.key()) :: boolean()
  def mem(%Distances{} = t, key), do: Map.has_key?(t.cells, key)

  @doc """
  Finds the shortest path from the `root` of the `Distances` struct to the `goal`
  cell. Returns `{:error, reason}` if there's no path.
  """
  @spec path_to(Distances.t(), Cell.key(), Grid.t()) :: Distances.t() | {:error, String.t()}
  def path_to(%Distances{} = t, {_, _} = goal, %Grid{} = grid) do
    distances = init(t.root)
    distances = put_in(distances.cells[goal], t[goal])
    path_to(t, goal, distances, grid)
  end

  defp path_to(%Distances{root: root}, root, %Distances{} = breadcrumbs, %Grid{}) do
    breadcrumbs
  end

  defp path_to(%Distances{} = t, {_, _} = current, %Distances{} = breadcrumbs, %Grid{} = grid) do
    Enum.find(Cell.links(grid[current]), fn neighbour ->
      t[neighbour] < t[current]
    end)
    |> case do
      nil ->
        {:error, "no path out of #{inspect(current)}"}

      neighbour ->
        breadcrumbs = put_in(breadcrumbs[neighbour], t[neighbour])
        path_to(t, neighbour, breadcrumbs, grid)
    end
  end

  @doc """
  Returns the furthest cell from the `root` and the distance to it.
  """
  @spec max(Distances.t()) :: {Cell.key(), non_neg_integer()}
  def max(%Distances{} = t) do
    for {cell, cell_distance} <- t.cells, reduce: {t.root, 0} do
      {furthest_cell, max_distance} ->
        if cell_distance <= max_distance,
          do: {furthest_cell, max_distance},
          else: {cell, cell_distance}
    end
  end
end
