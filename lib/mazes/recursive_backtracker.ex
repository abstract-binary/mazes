defmodule Mazes.RecursiveBacktracker do
  @moduledoc false
  @behaviour Mazes.Algorithm

  alias Mazes.{Cell, Grid}

  @impl true
  def run(%Grid{} = grid) do
    case Grid.random_cell(grid) do
      nil -> grid
      cell -> loop(grid, [cell])
    end
  end

  defp loop(%Grid{} = grid, []), do: grid

  defp loop(%Grid{} = grid, [%Cell{} = cell | rest] = stack) do
    case Cell.unvisited_neighbours(cell, grid) do
      [] ->
        loop(grid, rest)

      neighbours ->
        neighbour = Enum.random(neighbours)
        loop(Grid.link(grid, Cell.key(cell), neighbour), [grid[neighbour] | stack])
    end
  end
end
