defmodule Mazes.HuntAndKill do
  @moduledoc false
  @behaviour Mazes.Algorithm

  alias Mazes.{Cell, Grid}

  @impl true
  def run(%Grid{} = grid) do
    case Grid.random_cell(grid) do
      nil -> nil
      cell -> loop(grid, cell)
    end
  end

  defp loop(%Grid{} = grid, %Cell{} = cell) do
    case Cell.unvisited_neighbours(cell, grid) do
      [] ->
        hunt(grid)

      unvisited_neighbours ->
        neighbour = Enum.random(unvisited_neighbours)
        grid = Grid.link(grid, Cell.key(cell), neighbour)
        loop(grid, grid[neighbour])
    end
  end

  # Hunt for an unvisited cell with a visited neighbour.
  defp hunt(%Grid{} = grid) do
    grid
    |> Grid.cells()
    |> Enum.find_value(fn cell ->
      cell = grid[cell]
      visited_neighbours = Cell.visited_neighbours(cell, grid)

      if Enum.empty?(Cell.links(cell)) and not Enum.empty?(visited_neighbours),
        do: {cell, Enum.random(visited_neighbours)},
        else: nil
    end)
    |> case do
      nil ->
        grid

      {cell, neighbour} ->
        grid = Grid.link(grid, Cell.key(cell), neighbour)
        loop(grid, grid[neighbour])
    end
  end
end
