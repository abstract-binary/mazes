defmodule Mazes.Wilson do
  @moduledoc false
  @behaviour Mazes.Algorithm

  alias Mazes.{Cell, Grid}

  @impl true
  def run(%Grid{} = grid) do
    unvisited = grid |> Grid.cells() |> MapSet.new()
    first = Enum.random(unvisited)
    unvisited = MapSet.delete(unvisited, first)
    loop(grid, unvisited)
  end

  @spec loop(Grid.t(), MapSet.t()) :: Grid.t()
  defp loop(%Grid{} = grid, unvisited) do
    case MapSet.size(unvisited) do
      0 ->
        grid

      _ ->
        path = find_non_cyclic_path_from(grid, unvisited, [Enum.random(unvisited)])

        {grid, unvisited} =
          for {neighbour, cell} <- Enum.zip(path, tl(path)), reduce: {grid, unvisited} do
            {grid, unvisited} ->
              {Grid.link(grid, cell, neighbour), MapSet.delete(unvisited, cell)}
          end

        loop(grid, unvisited)
    end
  end

  @spec find_non_cyclic_path_from(Grid.t(), MapSet.t(), [Cell.key()]) :: [Cell.key()]
  defp find_non_cyclic_path_from(%Grid{} = grid, unvisited, path) do
    cell = hd(path)

    if MapSet.member?(unvisited, cell) do
      neighbour = Enum.random(Cell.neighbours(grid[cell]))

      case Enum.find_index(path, &(&1 == neighbour)) do
        nil ->
          find_non_cyclic_path_from(grid, unvisited, [neighbour | path])

        idx ->
          {_, new_path} = Enum.split(path, idx)
          find_non_cyclic_path_from(grid, unvisited, new_path)
      end
    else
      path
    end
  end
end
