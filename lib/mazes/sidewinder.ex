defmodule Mazes.Sidewinder do
  @moduledoc false
  @behaviour Mazes.Algorithm

  alias Mazes.{Cell, Grid}

  @impl true
  def run(%Grid{} = grid) do
    for row <- 0..(grid.rows - 1), reduce: grid do
      grid ->
        {grid, _} =
          for col <- 0..(grid.columns - 1), reduce: {grid, []} do
            {grid, run} ->
              cell = grid[{row, col}]
              run = [cell | run]

              at_eastern_boundary = is_nil(cell.east)
              at_northern_boundary = is_nil(cell.north)

              should_close_out =
                at_eastern_boundary || (not at_northern_boundary && :rand.uniform(2) == 2)

              if should_close_out do
                member = Enum.random(run)

                if is_nil(member.north),
                  do: {grid, []},
                  else: {Grid.link(grid, Cell.key(member), member.north), []}
              else
                {Grid.link(grid, Cell.key(cell), cell.east), run}
              end
          end

        grid
    end
  end
end
